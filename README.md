# 

# Optimización de imagenes

Optimice las imagenes entregadas para una mejor performance a la hora de bajar estos recursos

| Image              | Original Size | Final Size | Savings |
| ------------------ | ------------- | ---------- | ------- |
| ic_search.png      | 369 bytes     | 254 bytes  | 31.2%   |
| ic_search@2x.png   | 608 bytes     | 421 bytes  | 30.8%   |
| ic_shipping.png    | 567 bytes     | 314 bytes  | 44.6%   |
| ic_shipping@2x.png | 1KB           | 481 bytes  | 52.6%   |
| logo_ml.png        | 3KB           | 1.8KB      | 44.9%   |
| logo_ml@2x.png     | 8KB           | 3.1KB      | 61.1%   |
