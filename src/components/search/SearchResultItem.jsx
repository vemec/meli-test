// Module dependencies
import React from 'react';

/**
 * SearchResultItem
 */
class SearchResultItem extends React.Component {
  render() {

    // get free_shipping_icon icon
    let free_shipping_icon;
    if (true) {
      free_shipping_icon = <div className="result-item-shipping">&nbsp;</div>
    }

    return (
      <li className="result-item">
        <div className="result-item-container">
          <div className="result-item-image">
            <a href>
              <figure>
                <img src="image" alt="" />
              </figure>
            </a>
          </div>
          <div className="result-item-info">
            <div className="result-item-location">location</div>
            <div className="result-item-price">
              <span>currency amount </span>
              { free_shipping_icon }
            </div>
            <h2 className="result-item-title">
              <a href>
                title
              </a>
            </h2>
          </div>
        </div>
      </li>
    );
  }
}

export default SearchResultItem;
