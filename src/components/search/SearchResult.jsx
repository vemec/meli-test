// Module dependencies
import React from 'react';

// components
import Breadcrumb from '../categories/Breadcrumb.jsx';
import SearchResultItem from './SearchResultItem.jsx';
import EmptyState from '../EmptyState.jsx';

/**
 * SearchResult
 */
class SearchResult extends React.Component {
  render() {

    // not found item message
    let message = {
      title: 'No hay publicaciones que coincidan con tu búsqueda.',
      advices: [
        'Revisa la ortografía de la palabra.',
        'Utiliza palabras más genéricas o menos palabras.',
        'Navega por categorías de productos para encontrar un producto similar.'
      ]
    }

    // not found item message
    let categories = {
      categories: [
        '1',
        '2',
        '3'
      ]
    }

    // search result
    let search_result;
    if (true) {
      search_result =
        <div>
          <Breadcrumb categories={ categories } />
          <div className="search-result-container base-container">
            <ol>
              <SearchResultItem product={1} />
            </ol>
          </div>
        </div>
    }
    else {
      search_result = <EmptyState message={message} />;
    }

    return (
      <div>
        { search_result }
      </div>
    );
  }
}

export default SearchResult;
