// Module dependencies
import React from 'react';
import { connect } from 'react-redux'
import { browserHistory } from 'react-router'

/**
 * SearchForm
 */
class SearchForm extends React.Component {
  render() {
    return (
      <form className="search" action="a" method="GET" role="search">
        <input
               aria-label="Nunca dejes de buscar"
               placeholder="Nunca dejes de buscar"
               type="text"
               className="search-input"
               name="header-search"
               autoComplete="off"
               onKeyPress={ (e) => this.inputSearch(e) }
               ref={ input => this.textInput = input }
        />
        <button role="button" aria-label="Buscar" type="submit" className="search-btn" onClick={ (e) => this.submitQuery(e) }>
          <i className="search-icon"><span>Buscar</span></i>
        </button>
      </form>
    );
  }

  /**
   * submitQuery hace el search y cambia
   * la url con el termino buscado
   */
  submitQuery (e) {
  e.preventDefault();
    var user_query = this.textInput.value;
      if (user_query) {
        this.props._search(user_query);
        browserHistory.push("/items?search="+user_query);
    }
  }

  /**
   * inputSearch es un wrapper de submitQuery
   * solo que espera un "enter" del user
   * para realizar la busqueda
   */
  inputSearch(e) {
    if (e.key == 'Enter') {
      this.submitQuery(e);
    }
  }
}

export default SearchForm;
