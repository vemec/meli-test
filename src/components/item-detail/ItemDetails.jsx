// Module dependencies
import React from 'react';

// components
import Breadcrumb from '../categories/Breadcrumb.jsx';

/**
 * ItemDetails
 */
class ItemDetails extends React.Component {
  render() {

    // not found item message
    let categories = {
      categories: [
        '1',
        '2',
        '3'
      ]
    }

    return (
      <div>
        <Breadcrumb categories={ categories } />
        <div className="item-details-container base-container">
          <div className="item-top-data">
            <div className="item-image">
              <figure>
                <img src="https://placeholdit.co//i/680x560" alt="" />
              </figure>
            </div>
            <div className="item-short-desc">
              <div className="item-info">Nuevo - 2 Vendidos</div>
              <header className="item-title"><h1>Felt IA 14</h1></header>
              <div className="item-price">$ 45.000</div>
              <div className="item-pri-action">
                <button>Comprar</button>
              </div>
            </div>
          </div>
          <div className="item-description">
            <h2>Descripción del producto</h2>
            <div className="item-description-text">
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec scelerisque justo nec nisl venenatis imperdiet. Mauris a nulla at lectus venenatis gravida ac a lectus. Sed non hendrerit purus, quis viverra dui. Curabitur malesuada placerat felis, vitae lacinia felis. Nullam eleifend ex in eros iaculis molestie. Etiam lobortis scelerisque sem, at feugiat lectus tincidunt in. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ItemDetails;
