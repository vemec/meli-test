// Module dependencies
import React from 'react';

// components
import Header from './header/Header.jsx';
import EmptyState from './EmptyState.jsx';
import SvgIcons from './icons/SvgIcons.jsx';
import SearchResult from './search/SearchResult.jsx';

/**
 * App Layout
 */
class Layout extends React.Component {
  render() {

    // get search_result
    let message = {
      title: 'Escribe en el buscador lo que quieres encontrar',
      advices: [
        'Escribe tu búsqueda en el campo que figura en la parte superior de la pantalla.'
      ]
    }

    return (
      <div>
        <SvgIcons />
        <Header />
        <main role="main">
          <div className="content wrap-content">
            <EmptyState message={ message }/>
          </div>
        </main>
      </div>
    );
  }
}

export default Layout;
