import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { Router, browserHistory } from 'react-router'
import { createStore, applyMiddleware } from 'redux'

// components
import Layout from './components/Layout.jsx';

// import normalize.css
import 'normalize.css'

// import scss
import './scss/index.scss';

render(<Layout />, document.getElementById('app'));