const path = require('path');
const webpack = require('webpack');
const express = require('express');
const config = require('./webpack.config.js');

const app = express()
const compiler = webpack(config);

// index file to render
const index = path.join(__dirname, '/public/index.html')

// app.use(require('webpack-dev-middleware')(compiler, {
//   publicPath: config.output.publicPath
// }));

// app.use(require('webpack-hot-middleware')(compiler));

// Static files
app.use('/public', express.static(path.join(__dirname, 'public')))

// Home route
app.get('/', function (req, res) {
  res.sendFile(index)
})

// 404 error page
app.get('*', function(req, res) {
  res.status(404).sendFile(index)
});

// Listen to...
var server = app.listen(3000, function () {
  console.log('- MeliTest app listening on port %s!', server.address().port);
})
